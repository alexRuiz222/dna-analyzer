'user strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let DNA_schema = new Schema({
    dna: {
        type: [String],
        required: true
    },
    mutation: {
        type: Boolean,
        default: false,
        required: true
    }
}, { collection: 'mutations', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }, versionKey: false });

// DNA_schema.set('toObject', { transform: (doc, ret) => delete ret.__v });
mongoose.model('dna', DNA_schema).syncIndexes();

module.exports = mongoose.model('dna', DNA_schema);