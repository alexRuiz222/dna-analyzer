const server = require('./server');
const DNA_model = require('./dna.model');
module.exports = {
    server,
    DNA_model
}