// External libraries
const colors = require('colors');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const express = require('express');
const redoc = require('redoc-express');
const fs = require('fs');
const yml = require('js-yaml');

// Internal libraries
const { start_config } = require("../helpers");
const { db_connection } = require("../db/config");

// Server initial class
class server {
    constructor() {
        console.clear();
        this.app = express();

        //Router paths
        this.paths = {
                dnas: '/mutation', //DNA Validator
                stats: '/stats', // Stats request
            }
            //starting info API
        this.login();
    }

    async login() {
        // loading global variables
        global = await start_config();
        // setting port, "process.env.PORT " is for google engine
        this.port = process.env.PORT || global.api.port || 3000;
        // loading middlewares for the routes
        this.middlewares();
        // loading routes
        this.routes();
        // connection to database
        this.connectDB();
    }

    async connectDB() {
        try {
            await db_connection();
        } catch (error) {
            console.log(error);
            await new Promise(resolve => setTimeout(resolve, 5000));
            return this.connectDB();
        }
        await this.listen();
    }

    middlewares() {
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.static('public'));
    }


    routes() {
        this.app.use(this.paths.dnas, require('../routes/dnas.route'));
        this.app.use(this.paths.stats, require('../routes/stats.route'));
        // Documentation
        this.app.get('/docs/doc.yml', (req, res) => {
            res.sendFile('doc.yml', { root: '.' });
        });

        this.app.get('/docs',
            redoc({
                title: 'DNA ANALYSER - Documentations',
                specUrl: '/docs/doc.yml'
            })
        );

        // Info api
        this.app.get('/about', (req, res) => {
            // read.js
            try {
                let fileContents = fs.readFileSync('./doc.yml', 'utf8');
                let data = yml.safeLoad(fileContents);

                // console.log(data);
                return res.status(200).json({
                    // name: data.info.title,
                    version: data.info.version
                });
            } catch (e) {
                console.log(e);
            }
        });
    }


    // Listen to redis
    async redis() {

        if (global.redis.enabled) {

            await redis_purge();
            redis_documents();

        }

    }

    // Execution port
    async listen() {
        this.app.listen(this.port, () => {
            console.log(` ================================================= WELCOME TO ${'DNA ANALYSER'.underline} API ========================================`.italic.brightMagenta);
            console.log(`\n        💻 PORT = ${this.port} `.italic.yellow, `|`.brightMagenta, ` ✔ = SUCCESSFULLY EXECUTION `.italic.green, `|`.brightMagenta, ` ❌ = SOMETHING WRONG `.italic.red, `|`.brightMagenta, ` 👽 = EXTRA INFORMATION`.italic.cyan);
            console.log(`\n ======================================================================================================================`.brightMagenta);
            console.log(`                                             ✨ HERE COMES ALL THE MAGIC ✨                               `.brightYellow);
            // console.log(` ----------------------------------------------------------------------------------------------------------------------`.rainbow);
            // console.log();
        });
    }
}

module.exports = server;