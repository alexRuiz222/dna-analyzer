require("colors");
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);
const url = `http://localhost:3000`;
console.clear();
let data_1 = {
    "dna": ["AGTTTA", "CAGGAC", "TGATGT", "AGAAGG", "CACCTA", "TCACTG"]
}
let data_2 = {
    "dna": ["AGATTA", "CATGAC", "TGATGT", "AGAGGG", "CACCTA", "TCACTG"]
}
let data_3 = {
    "dna": ["AGAXTA", "CAFGAC", "TGXTGT", "AGAGGG", "CACCTA", "TCACTG"]
}

describe('DNA ANALYZER:'.brightBlue, () => {
    it('POST'.brightYellow + ' - Must be true'.white, (done) => {
        chai.request(url)
            .post('/mutation')
            .send(data_1)
            .end((err, res) => {
                // expect(res.body).to.be.true;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('POST'.brightYellow + ' - Must be false'.white, (done) => {
        chai.request(url)
            .post('/mutation')
            .send(data_2)
            .end((err, res) => {
                // expect(res.body).to.be.false;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('POST'.brightYellow + ' - Must be wrong data 403'.white, (done) => {
        chai.request(url)
            .post('/mutation')
            .send(data_3)
            .end((err, res) => {
                // expect(res.body).to.be.false;
                expect(res).to.have.status(403);
                done();
            });
    });
});