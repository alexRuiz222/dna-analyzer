const { response } = require("express");
const { prettyLog } = require('../helpers');
const { DNA_model } = require('../models');

/* -------------------------          ✨ HERE COMES ALL THE MAGIC ✨             ---------------------------------------------- */

/** This function return an JSON stats from the dna's on the database */
const GET_STATS = async(req, res = response) => {
    // try catch
    try {
        // Find mutation registries
        let query = { mutation: true };
        const [TOTAL_MUTATIONS] = await Promise.all([
            DNA_model.countDocuments(query),
            DNA_model.find(query)
        ]);

        // Find no mutation registries
        query.mutation = false;
        const [TOTAL_NO_MUTATIONS] = await Promise.all([
            DNA_model.countDocuments(query),
            DNA_model.find(query)
        ]);

        //console log for information
        prettyLog('ok', { text: 'GET TOTAL_MUTATIONS', total: TOTAL_MUTATIONS });
        prettyLog('ok', { text: 'GET TOTAL_NO_MUTATIONS', total: TOTAL_MUTATIONS });

        // response 
        return res.status(200).json({
            count_mutations: TOTAL_MUTATIONS,
            count_no_mutations: TOTAL_NO_MUTATIONS,
            ratio: TOTAL_MUTATIONS / TOTAL_NO_MUTATIONS
        });

    } catch (err) {
        //console log for the error to easily debug
        console.log(err);
        return res.status(403).json({
            param: "search",
            location: "params",
            msg: "Invalid search",
            msg_es: "Búsqueda inválida"
        });
    }
}

/**
 this function have to analyze an DNA to return a boolean, depends on dna is mutable or not
 */

// DNA EXAMPLE 
// " A G T T A A ",
// " C A G A A C ",
// " T G A A G T ",
// " A G A A G G ",
// " C A C C T A ",
// " T C A C T G "

const POST_MUTATION = async(req, res = response) => {
    const { dna } = req.body;
    if (!await CHECK_STRNG(dna)) {
        return res.status(403).json({
            msg: 'Error analyzing dna',
            msg_es: 'Error al analizar el ADN'
        });
    }
    //Calling the checkers for the dna validation, vertical - horizontal - diagonal
    let mutation = (CHECK_VERTICAL(dna) + CHECK_HORIZONTAL(dna) + CHECK_DIAGONAL(dna)) > 1;
    let data = { dna, mutation };
    try {
        const ALREADY_EXISTS = await DNA_model.findOne({ dna: data.dna });
        // If the dna was registred before, it does not gonna be inserted
        if (!ALREADY_EXISTS) {
            const NEW_DNA = new DNA_model(data);
            await NEW_DNA.save();
        }

        //console log for information
        prettyLog('ok', { text: 'POST DNAS' });

        return res.status(200).json(mutation);
    } catch (err) {
        console.log(err);
        return res.status(403).json({
            msg: 'Error analyzing dna',
            msg_es: 'Error al analizar el ADN',
            err
        });
    }
}

//check if is vertical repeated
const CHECK_VERTICAL = (DNA) => {
    let mutations_found = 0,
        repetitions = 1,
        prev = '';

    for (let x = 0; x < DNA[0].length; x++) {
        for (let y = 0; y < DNA.length; y++) {
            let actual = DNA[y][x];
            if (prev != '') {
                repetitions = actual == prev ? repetitions + 1 : 1;
                if (repetitions == 4) mutations_found++;
            }
            prev = actual;
        }

    }
    return mutations_found;
}

//check if is horizontal repeated
const CHECK_HORIZONTAL = (DNA) => {
    let mutations_found = 0,
        repetitions = 1;
    DNA.forEach(arr => {
        repetitions = 1;
        for (let x = 0; x < arr.length; x++) {
            const letter = arr[x];
            if (x > 0) {
                repetitions = letter == arr[x - 1] ? repetitions + 1 : 1;
                if (repetitions == 4) mutations_found++;
            }
        }
    });
    return mutations_found;
}

//check if is diagonal repeated
const CHECK_DIAGONAL = (DNA) => {
    let mutations_found = 0,
        repetitions = 1,
        prev = '';

    //From left to right
    for (let x = 0; x < DNA[0].length; x++) {
        let actual = DNA[x][x];
        if (prev != '') {
            repetitions = actual == prev ? repetitions + 1 : 1;
            if (repetitions == 4) mutations_found++;
        }
        prev = actual;
    }

    //From right to left
    repetitions = 1, prev = '';
    let y = 0;
    for (let x = DNA[0].length - 1; x >= 0; x--) {
        let actual = DNA[y][x];
        if (prev != '') {
            repetitions = actual == prev ? repetitions + 1 : 1;
            if (repetitions == 4) {
                mutations_found++;
            }
        }
        prev = actual;
        y++;
    }

    return mutations_found;
}

// check if there are just ATCG CHARS
const CHECK_STRNG = (DNA) => {
    for (const el of DNA) {
        if (el.split('').filter(c => c != 'A' && c != 'T' && c != 'C' && c != 'G').length > 0) return false;
    }
    return true;
}

/* -------------------------                ✨ EXPORTING  MAGIC ✨                ---------------------------------------------- */

module.exports = { GET_STATS, POST_MUTATION };