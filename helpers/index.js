const start_config = require('./start_config');
const tools = require('./tools');
module.exports = {
    ...start_config,
    ...tools
}