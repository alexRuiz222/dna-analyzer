const isValidJson = (json) => {
    try {
        JSON.parse(json);
        return true;
    } catch (e) {
        return false;
    }
};

const sensitiveRegex = async(string = '') => {
    return string.toLowerCase()
        .replace(/[aâäàåá]/g, '[a,â,ä,à,å,á]')
        .replace(/[eêëèé]/g, '[e,ê,ë,è,é]')
        .replace(/[iïîìí]/g, '[i,ï,î,ì,í]')
        .replace(/[oôöòó]/g, '[o,ô,ö,ò,ó]')
        .replace(/[uüûùú]/g, '[u,ü,û,ù,ú]')
        .replace(/[nñ]/g, '[n,ñ]');
}

const escapeStringRegexp = (string) => {
    if (typeof string !== 'string') {
        return string;
    }
    return string.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&').replace(/-/g, '\\x2d');
}

const arrayToObject = (arreglo, expresion) => {
    const buscarEn = [];
    let buscar = [];
    const white_list = [];
    for (let i = 0; i < arreglo.length; ++i) {
        let rv = {};
        if (arreglo[i] !== undefined) {
            buscar = arreglo[i].split('.');

            if (buscar.length > 1 && white_list.includes(buscar[0].toString())) {
                rv[buscar[0].toString()] = {
                    $elemMatch: {
                        [buscar[1].toString()]: expresion,
                        status: true
                    }
                };

            } else {
                rv[arreglo[i].toString()] = expresion;
            }
            buscarEn.push(rv);
        }

    }

    return buscarEn;

};

const prettyLog = (type, item) => {
    console.log(` ----------------------------------------------------------------------------------------------------------------------`.rainbow);
    switch (type) {
        case 'ok':
            console.log(` ✔ ${item.text.toUpperCase()} `.italic.green, item.total ? `|`.brightMagenta + ` 👽 TOTAL DATA FOUND: ${item.total} `.italic.cyan : '', `|`.brightMagenta, ` 📅: ${new Date()} `.italic.blue);
            break;
        case 'error':
            console.log(` ❌ ${item.text.toUpperCase()} `.italic.red);
            break;
    }
}

module.exports = { isValidJson, sensitiveRegex, escapeStringRegexp, arrayToObject, prettyLog };