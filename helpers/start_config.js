// Internal tools 
const path = require('path');
const fs = require('fs');
const colors = require('colors');

let config = {};

const start_config = async() => {
    let url = path.join(__dirname, '../config/config.json');
    if (await fs.existsSync(url)) {
        config = await fs.readFileSync(url, 'utf8');
        config = JSON.parse(config);
    }
    global = {...config };
    global.mongo = config.mongo ? config.mongo : mongo_default();
    global.api = config.api ? config.api : api_default();
    global.security = config.security ? config.security : undefined;
    return global;
}

// Mongo default configuration
const mongo_default = () => {
    return {
        enabled: true,
        use_defaults: true,
        srv: false,
        host: "localhost",
        port: 27017,
        user: "",
        pass: "",
        db: "dna_analyser"
    };
}

// Api default configuration
const api_default = () => {
    return {
        port: 3000
    };
}



module.exports = { start_config };