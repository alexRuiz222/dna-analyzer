const { response } = require('express');
const { validationResult } = require('express-validator');

const validator_fields = (req, res = response, next) => {
    const errors = validationResult(req).formatWith(({ location, msg, param, value, nestedErrors }) => {
        return { location, msg_es: msg.msg_es, msg: msg.msg, param, value, nestedErrors };
    });

    if (!errors.isEmpty()) {
        return res.status(403).json({ errors: errors.array() });
    }

    next();

};

module.exports = { validator_fields };