const db = require("./db");
const validator_fields = require("./validator_fields");
const exists_by_id = require("./exists_by_id")

module.exports = {
    ...validator_fields,
    ...db,
    ...exists_by_id
}