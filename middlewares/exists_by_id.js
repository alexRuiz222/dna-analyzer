const { SNA_model } = require("../models");

const exists_dna_by_id = async(_id, info) => {
    const dnaExists = await SNA_model.findOne({ _id: _id, info: info });
    if (!dnaExists || !dnaExists.status) {
        throw {
            param: "_id",
            location: "params",
            msg: "This Id doesn't exists",
            msg_es: `El ID no existe`
        };
    }
}


module.exports = { exists_dna_by_id };