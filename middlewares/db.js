const mongoose = require('mongoose');
mongoose instanceof mongoose.Mongoose;

const db_connection = async(req, res, next) => {
    try {
        mongoose.connection.readyState == 1 ? next() : res.status(400).json({ msg: 'Error connecting to database', msg_es: 'Error conectando a la base de datos' });
    } catch (error) {
        console.log(error);
        return res.status(400).json(error);
    }
}

module.exports = { db_connection };