const mongoose = require('mongoose');
const db_connection = async() => {
    return await new Promise(async(resolve, reject) => {
        try {
            await mongoose.connect(`mongodb${global.mongo.srv ? '+srv':''}://${((global.mongo.user !== '' && global.mongo.user !== undefined) ? global.mongo.user + ':' + ((global.mongo.pass !== '' && global.mongo.pass !== undefined) ? global.mongo.pass + '@' : '@') : '')}${global.mongo.host}${((!global.mongo.srv) ? ':' + global.mongo.port : '')}/${global.mongo.db}`, {

            }, (err) => {
                if (err) {
                    console.log(err);
                    return reject({ msg: 'Error connecting to database', msg_es: 'Error conectando a la base de datos' });
                }
                return resolve("OK");
            });
        } catch (error) {
            console.log(err);
            return reject({ msg: 'Error connecting to database', msg_es: 'Error conectando a la base de datos' });
        }
    })
}

module.exports = { db_connection }