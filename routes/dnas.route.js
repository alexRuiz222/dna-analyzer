// External validators
const { Router } = require('express');
const { check } = require('express-validator');
const { POST_MUTATION } = require('../controllers/dna.controller');
const { db_connection, validator_fields } = require('../middlewares');

const router = Router();
// Routes
router.post('/', [
    db_connection,
    check('dna', { msg: 'There are not dna to analyze', msg_es: 'No hay ADN para analizar' }).not().isEmpty(),
    validator_fields,
], POST_MUTATION);

module.exports = router;