// External validators
const { Router } = require('express');
const { GET_STATS } = require('../controllers/dna.controller');
const { db_connection, validator_fields } = require('../middlewares');

const router = Router();
// Routes
router.get('/', [
    db_connection
], GET_STATS);

module.exports = router;