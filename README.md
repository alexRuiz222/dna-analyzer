# DNA-ANALYZER API

This is an API to analyze if a DNA is mutable or not

## For better documentation let see

    http://localhost:3000/docs/

## Usage

You can use the API from the cloud

    https://dna-analyzer-354822.uc.r.appspot.com/stats

## Installation

This API requires [Node.js](https://nodejs.org/) v14+ to run.

## Install

    npm install

## Run the app

    npm start

## Run the tests

    npm test

# REST API

Whit this API you will know if a DNA have a mutation and then get the stats

## Get stats

(Fetch example)

### Request

`GET /stats/`

     let headersList = {
    "Accept": "*/*"
    }

    fetch("localhost:3000/stats", {
    method: "GET",
    headers: headersList
    }).then(function(response) {
    return response.text();
    }).then(function(data) {
    console.log(data);
    })

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 2
    {
        "count_mutations": 2,
        "count_no_mutations": 1,
        "ratio": 2
    }

## Get mutation from a DNA

(Fetch example)

### Request

`POST /mutation/`

    let headersList = {
        "Accept": "*/*",
        "Content-Type": "application/json"
        }

        let bodyContent = JSON.stringify({
            "dna":["AGTTTA","CAGGAC","TGATGT","AGAAGG","CACCTA","TCACTG"]
        });

        fetch("localhost:3000/mutation", {
        method: "POST",
        body: bodyContent,
        headers: headersList
        }).then(function(response) {
            return response.text();
        }).then(function(data) {
            console.log(data);
        })

### Response

    HTTP/1.1 200 Created
    Date: Thu, 24 Feb 2011 12:36:31 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json
    Location: /thing/2
    Content-Length: 35

    true
